#!/usr/bin/python3
#+
# Darlink example: an implementation of a modified version of the
# org.example.ftl interface from the Varlink home page. This one uses
# the higher-level larvink interface, with support for introspection.
#
# The caller-visible state of the spaceship comprises its current
# position, its fuel level and the state of its engines (idle or
# busy). Method calls are provided to calculate parameters to move
# to a specified position, and also to initiate such movement.
# Plus there is a Monitor call, which triggers an endless stream
# of periodic state updates, which can only be stopped by closing
# the connection.
#
# Position coordinates are specified in terms of degrees longitude and
# latitude (with the 0° meridian in line with our Solar System), and
# distance in light-years from the centre of the Milky Way galaxy.
#
# Speed is in units of light-years per time-unit. In this simulation,
# to keep things reasonable, one time-unit passes per second. But the
# time unit might represent something else in simulated ship time,
# e.g. one Earth day.
#
# Copyright 2024 by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>. This
# script is licensed CC0
# <https://creativecommons.org/publicdomain/zero/1.0/>; do with it
# what you will.
#-

import sys
import io
import math
import enum
import time
import traceback
import asyncio
import larvink as lv

#+
# Navigation
#-

DEG = math.pi / 180
  # a single conversion factor provides conversions between radians
  # (the natural unit for trig functions) and a specified angle unit;
  # in this case, degrees.

def to_cartesian(long, lat, distance) :
    "converts angular+radial coordinates to Cartesian coordinates."
    # These coords are only used internally.
    x = distance * math.cos(long * DEG) * math.cos(lat * DEG)
    y = distance * math.sin(long * DEG) * math.cos(lat * DEG)
    z = distance * math.sin(lat * DEG)
    return (x, y, z)
#end to_cartesian

# limit decimal places exposed in Varlink interface
distance_round = 3
angle_round = 6

def from_cartesian(x, y, z) :
    "converts Cartesian coordinates to angular+radial coordinates."
    # These coords are returned to caller; round things to avoid having too many digits
    distance = round(math.hypot(x, y, z), distance_round)
    long = round(math.atan2(y, x) / DEG, angle_round)
    lat = round(math.atan2(z, math.hypot(x, y)) / DEG, angle_round)
    return \
        (long, lat, distance)
#end from_cartesian

#+
# Task management
#-

tasks = set()
  # keep strong references to currently-live tasks

def task_done(task) :
    "reports anything untoward about a task termination."
    if not task.cancelled() :
        err = task.exception()
        if err != None :
            sys.stderr.write("task “%s” terminated with “%s”\n" % (task.get_name(), str(err)))
            trc = io.StringIO()
            traceback.print_exception(None, err, err.__traceback__, file = trc)
            for line in trc.getvalue().split("\n") :
                sys.stderr.write("trc> %s\n" % line)
            #end for
        else :
            sys.stderr.write("task “%s” terminated.\n" % task.get_name())
        #end if
    #end if
    tasks.discard(task)
#end task_done

def create_task(coro, name) :
    task = asyncio.create_task(coro, name = name)
    task.add_done_callback(task_done)
    tasks.add(task)
#end create_task

#+
# Vehicle state
#-

# cannot exceed region of Milky Way
max_radius = 50000 # max distance from centre along thick dimensions
max_lateral = 1000 # max distance along thin dimension

max_speed = 100
max_range = 10000 # pitiful, I know

def calculate_delta(currentpos, targetpos, err_parameter_out_of_range) :
    "returns the delta in Cartesian coordinates to travel from currentpos to target."
    current = to_cartesian \
        (currentpos["longitude"], currentpos["latitude"], currentpos["distance"])
    target = to_cartesian(targetpos["longitude"], targetpos["latitude"], targetpos["distance"])
    sys.stderr.write("Cartesian: current = %s, target = %s\n" % (current, target)) # debug
    if (
        # constrain coords to something resembling dimensions of Milky Way galaxy
            math.hypot(current[0], current[1]) > max_radius
        or
            abs(current[2]) > max_lateral
        or
            math.hypot(target[0], target[1]) > max_radius
        or
            abs(target[2]) > max_lateral
    ) :
        err_parameter_out_of_range(field = "distance")
    #end if
    delta = tuple(xt - xc for xc, xt in zip(current, target))
    return delta
#end calculate_delta

#+
# Mainline
#-

ADDRESS = "unix:@ftl_spaceship"

@lv.interface("org.example.ftl")
class FTLSpaceship :

    __slots__ = ("currentpos", "fuel_level", "travelling")

    # Some struct fields are int when they would more sensibly be float.
    # Think of this as just exercising the type-interface logic.

    CoordsType = lv.structtype \
      (
        name = "Coordinates",
        fields =
            [
                ("longitude", float),
                ("latitude", float),
                ("distance", float),
            ]
      )

    DriveConfigType = lv.structtype \
      (
        name = "DriveConfiguration",
        fields =
            [
                ("speed", int),
                ("trajectory_long", float),
                ("trajectory_lat", float),
                ("duration", int),
            ]
      )

    @lv.enumtype(instance_name = lambda e : e.label)
    class DRIVESTATE(enum.Enum) :
        IDLE = ("idle", "not currently going anywhere")
        BUSY = ("busy", "on our way")

        @property
        def label(self) :
            return self.value[0]
        #end label

        @property
        def description(self) :
            return self.value[1]
        #end description

    #end DRIVESTATE

    DriveConditionType = lv.structtype \
      (
        name = "DriveCondition",
        fields =
            [
                ("state", DRIVESTATE),
                ("tylium_level", int),
            ]
      )

    def __init__(self) :
        self.currentpos = \
            { # start from Sun/Earth position
                "longitude" : 0.0, # by definition
                "latitude" : 0.117,
                  # 55 light years from main plane, putting ourselves in “northern” half
                "distance" : 27000.0, # from centre
            }
        self.fuel_level = max_range # to start with
        self.travelling = False
    #end __init__

    @lv.method \
      (
        name = "Monitor",
        inargs = [],
        outargs =
            [
                ("condition", DriveConditionType),
                ("position", CoordsType),
            ],
        want_more = True
      )
    async def monitor(self) :
        "sends a stream of ship-status messages back to the client."
        celf = type(self)
        DriveConditionType = celf.DriveConditionType
        DRIVESTATE = celf.DRIVESTATE
        while True :
            yield \
              (
                {
                    "condition" :
                        DriveConditionType
                          (
                            state = (DRIVESTATE.IDLE, DRIVESTATE.BUSY)[self.travelling],
                            tylium_level = self.fuel_level
                          ),
                    "position" : self.currentpos,
                },
                True
              )
            await asyncio.sleep(1.0)
        #end while
    #end monitor

    async def travel(self, delta, speed, duration) :
        "simulates travel in the specified direction vector at the specified" \
        " speed for the specified duration."
        # Note this method is not exposed in the Varlink interface.
        assert not self.travelling
        self.travelling = True
        start_time = time.monotonic()
        start_fuel_level = self.fuel_level
        startpos = to_cartesian \
          (
            self.currentpos["longitude"],
            self.currentpos["latitude"],
            self.currentpos["distance"]
          )
        try :
            while True :
                # update position and consume fuel
                time_so_far = min(time.monotonic() - start_time, duration, start_fuel_level / speed)
                pos = tuple(startpos[i] + time_so_far * delta[i] for i in range(3))
                pos = from_cartesian(*pos)
                self.currentpos = dict(zip(("longitude", "latitude", "distance"), pos))
                self.fuel_level = max(round(start_fuel_level - time_so_far * speed), 0)
                if self.fuel_level == 0 or time_so_far >= duration :
                    break
                await asyncio.sleep(1.0)
            #end while
        finally :
            self.travelling = False
        #end try
    #end travel

    @lv.method \
      (
        name = "CalculateConfiguration",
        inargs =
            [
                ("target", CoordsType),
            ],
        inargs_keys = ["targetpos"],
        outargs =
            [
                ("configuration", DriveConfigType),
            ],
      )
    def calculate_trajectory(self, targetpos) :
        delta = calculate_delta(self.currentpos, targetpos, self.err_parameter_out_of_range)
        distance = round(math.hypot(*delta))
        if distance > self.fuel_level :
            # too far for our drive to go ...
            self.err_parameter_out_of_range(field = "delta_distance")
        #end if
        if distance > 0 :
            # some arbitrary restrictions on not immediately
            # heading to destination at maximum speed ...
            speed = round(math.sqrt(distance))
            duration = round(distance / speed)
            trajectory_long = \
                round(math.atan2(delta[1], delta[0]) / DEG, angle_round)
            trajectory_lat = round \
              (
                math.atan2(delta[2], math.hypot(delta[0], delta[1])) / DEG,
                angle_round
              )
        else :
            speed = duration = trajectory_long = trajectory_lat = 0
        #end if
        return \
            {
                "configuration" :
                    type(self).DriveConfigType
                      (
                        speed = speed,
                        trajectory_long = trajectory_long,
                        trajectory_lat = trajectory_lat,
                        duration = duration
                      ),
            }
    #end calculate_trajectory

    @lv.method \
      (
        name = "Jump",
        inargs =
            [
                ("configuration", DriveConfigType),
            ],
        inargs_keys = ["trajectory"],
        outargs = [],
      )
    def start_travel(self, trajectory) :
        "starts a journey along the specified course, speed and duration."
        if trajectory["speed"] <= 0 or trajectory["speed"] > max_speed :
            self.err_parameter_out_of_range(field = "speed")
        #end if
        if trajectory["duration"] < 0 :
            self.err_parameter_out_of_range(field = "duration")
        #end if
        if self.travelling :
            self.err_jump_in_progress()
        #end if
        delta = to_cartesian(trajectory["trajectory_long"], trajectory["trajectory_lat"], trajectory["speed"])
        create_task \
          (
            self.travel(delta, trajectory["speed"], trajectory["duration"]),
            name = "travel task"
          )
        return {}
    #end start_travel

    # errors:
    err_parameter_out_of_range = lv.error("ParameterOutOfRange", [("field", str)])
    err_jump_in_progress = lv.error("JumpInProgress", ())

#end FTLSpaceship

async def mainline() :
    conn = await lv.ConnectionAsync(bind = ADDRESS)
    conn.register \
      (
        lv.IntrospectionHandler
          (
            vendor = "Superluminal Systems, Inc",
            product = "FTL Spaceship",
            version = "1.0",
            url = "https://varlink.org"
          )
      )
    conn.register(FTLSpaceship)
    await conn.closing
#end mainline

asyncio.run(mainline())
