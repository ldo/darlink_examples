#!/usr/bin/python3
#+
# Darlink example: monitor the condition of the spaceship server.
# This version uses async/await.
#
# Copyright 2024 by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>. This
# script is licensed CC0
# <https://creativecommons.org/publicdomain/zero/1.0/>; do with it
# what you will.
#-

import sys
import asyncio
import darlink as dl

ADDRESS = "unix:@ftl_spaceship"

async def mainline() :
    conn = await dl.ConnectionAsync(connect = ADDRESS)
    async for status in conn.transact \
      (
        req = dl.Message.create_method(method = "org.example.ftl.Monitor", more = True)
      ) \
    :
        sys.stdout.write(repr(status.parameters))
        sys.stdout.write("\n")
    #end for
#end mainline

asyncio.run(mainline())
